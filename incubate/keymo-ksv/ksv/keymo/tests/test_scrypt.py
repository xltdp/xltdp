import unittest
from keymo.api.symmetric.scrypt import SCRYPT

class TestSCRYPT(unittest.TestCase):
    def setUp(self):
        self.password = "password"
        self.salt = b"salt"
        self.N = 16384
        self.r = 8
        self.p = 1
        self.message = "secret message"
        self.s = SCRYPT(self.password, self.salt, self.N, self.r, self.p)

    def test_encrypt(self):
        encrypted = self.s.encrypt(self.message)
        self.assertNotEqual(encrypted, self.message)
        self.assertEqual(len(encrypted), len(self.message) + 64)

    def test_decrypt(self):
        encrypted = self.s.encrypt(self.message)
        decrypted = self.s.decrypt(encrypted)
        self.assertEqual(decrypted, self.message)

    def test_generate_key(self):
        key = self.s.generate_key()
        self.assertEqual(len(key), 64)

    def test_verify(self):
        encrypted = self.s.encrypt(self.message)
        self.assertTrue(self.s.verify(encrypted))
        encrypted = encrypted[:-1]
        self.assertFalse(self.s.verify(encrypted))