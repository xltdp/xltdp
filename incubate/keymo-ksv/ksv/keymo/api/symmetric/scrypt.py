from cryptography.hazmat.primitives.kdf.scrypt import Scrypt
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.backends import default_backend

class SCRYPT:
    def __init__(self, password, salt, N=16384, r=8, p=1):
        self.password = password.encode() if isinstance(password, str) else password
        self.salt = salt.encode() if isinstance(salt, str) else salt
        self.N = N
        self.r = r
        self.p = p
        self.backend = default_backend()

    def _create_kdf(self):
        return Scrypt(
            salt=self.salt,
            length=64,
            n=self.N,
            r=self.r,
            p=self.p,
            backend=self.backend
        )

    def encrypt(self, message):
        kdf = self._create_kdf()
        key = kdf.derive(self.password)
        return message.encode() + key
    
    def decrypt(self, encrypted_message):
        kdf = self._create_kdf()
        key = kdf.derive(self.password)
        return encrypted_message[:-64].decode()
    
    def generate_key(self):
        kdf = self._create_kdf()
        return kdf.derive(self.password)
    
    def verify(self, encrypted_message):
        kdf = self._create_kdf()
        key = kdf.derive(self.password)
        return encrypted_message.endswith(key)