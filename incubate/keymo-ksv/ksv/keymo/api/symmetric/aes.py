from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import padding, serialization
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes

class AES:
    def __init__(self, key=None, mode=modes.CBC):
        if key:
            self.key = key
        else:
            self.key = os.urandom(32)
        self.mode = mode

    def generate_key(self, key_size=256):
        if key_size >= 512 and self.mode in (modes.GCM, modes.CCM):
            self.key = os.urandom(64)
        else:
            self.key = os.urandom(32)

    def encrypt(self, data):
        data = self._pad(data)

        cipher = Cipher(algorithms.AES(self.key), self.mode(self.key), backend=default_backend())
        encryptor = cipher.encryptor()
        ciphertext = encryptor.update(data) + encryptor.finalize()

        return ciphertext

    def decrypt(self, ciphertext):
        cipher = Cipher(algorithms.AES(self.key), self.mode(self.key), backend=default_backend())
        decryptor = cipher.decryptor()
        data = decryptor.update(ciphertext) + decryptor.finalize()

        return self._unpad(data)

    def export_key(self):
        return self.key

    def import_key(self, key):
        self.key = key

    def set_mode(self, mode):
        self.mode = mode

    def _pad(self, data):
        padder = padding.PKCS7(128).padder()
        padded_data = padder.update(data) + padder.finalize()
        return padded_data

    def _unpad(self, padded_data):
        unpadder = padding.PKCS7(128).unpadder()
        data = unpadder.update(padded_data) + unpadder.finalize()
        return data