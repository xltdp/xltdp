from simpleverify import SimpleVerify

# Usage example 
kdf = SimpleVerify(b"password", b"salt", 100000, 32, hashes.SHA256)
key = kdf.derive_key()
password_to_check = b"password"
key_to_check = kdf.derive_key()

if kdf.verify_key(password_to_check, key_to_check):
    print("The key is correct")
else:
    print("The key is incorrect")