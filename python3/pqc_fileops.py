# Import the PostQuantumCiphers class
from postquantum_ciphers import PostQuantumCiphers

class PQCFileOps:
    def __init__(self, password, salt, algorithm):
        # Create a PostQuantumCiphers object with the password, salt, and algorithm
        self.cipher = PostQuantumCiphers(password, salt, algorithm)

    def encrypt_file(self, input_file, output_file, salt):
        # Read the contents of the file to be encrypted
        with open(input_file, 'r') as f:
            data = f.read()

        # Encrypt the data
        ciphertext = self.cipher.encrypt(data, salt)

        # Write the ciphertext to a file
        with open(output_file, 'wb') as f:
            f.write(ciphertext)

    def decrypt_file(self, input_file, output_file, salt):
        # Read the ciphertext from the file
        with open(input_file, 'rb') as f:
            ciphertext = f.read()

        # Decrypt the ciphertext
        data = self.cipher.decrypt(ciphertext, salt)

        # Write the decrypted data to a file
        with open(output_file, 'w') as f:
            f.write(data)
