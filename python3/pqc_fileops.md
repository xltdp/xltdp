# pqc/fileops
This class has two methods: `encrypt_file` and `decrypt_file`. The `encrypt_file` method reads the contents of the specified input file, encrypts it using` NewHope`, and writes the resulting ciphertext to the specified output file. The `decrypt_file` method reads the ciphertext from the specified input file, decrypts it using NewHope, and writes the resulting decrypted data to the specified output file.
  
To use this class, you can do the following:
```
# Import the PQCFileOps class
from pqc_fileops import PQCFileOps

# Create a PQCFileOps object with the password, salt, and algorithm
pqc = PQCFileOps('password', 'salt', 'newhope')

# Encrypt a file
pqc.encrypt_file('data.txt', 'ciphertext.bin', 'salt')

# Decrypt a file
pqc.decrypt_file('ciphertext.bin', 'decrypted.txt', 'salt')
```
This code creates a `PQCFileOps` object with the specified password, salt, and algorithm, and then uses it to encrypt the file `data.txt` and write the resulting ciphertext to the file `ciphertext.bin`, and then decrypt the file `ciphertext.bin` and write the resulting decrypted data to the file `decrypted.txt`.  