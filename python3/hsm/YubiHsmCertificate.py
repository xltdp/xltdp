import datetime
import yubihsm

from cryptography import x509
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.x509.oid import NameOID

class Certificate:
    def __init__(self, hsm, object_id, domain, label, algorithm, certificate, signer_key_id):
        self.hsm = hsm
        self.object_id = object_id
        self.domain = domain
        self.label = label
        self.algorithm = algorithm
        self.certificate = certificate
        self.signer_key_id = signer_key_id

    # A class method is created using the @classmethod decorator. It takes the class itself as the first argument, rather than the instance of the class (self).
    @classmethod
    def generate_root_certificate(cls, hsm, object_id, domain, label, algorithm, signer_key_id, validity=None):
        if validity is None:
            validity = (0, 0)

        # Get the public key of the signer key
        signer_key = yubihsm.YubiHsmKey.get(hsm, signer_key_id, domain)
        signer_public_key = signer_key.get_public_key()

        # Build the root certificate
        builder = x509.CertificateBuilder()
        builder = builder.serial_number(x509.random_serial_number())
        builder = builder.issuer_name(x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, label)
        ]))
        builder = builder.subject_name(x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, label)
        ]))
        builder = builder.not_valid_before(datetime.datetime.utcnow())
        if validity[1] == 0:
            builder = builder.not_valid_after(datetime.datetime.max)
        else:
            builder = builder.not_valid_after(datetime.datetime.utcfromtimestamp(validity[1]))
        builder = builder.public_key(signer_public_key)
        builder = builder.add_extension(x509.BasicConstraints(ca=True, path_length=None), critical=True)

        # Sign the certificate
        certificate = builder.sign(
            private_key=signer_key.get_private_key(),
            algorithm=hashes.SHA256(),
            backend=yubihsm.backend
        )

        # Create and return the certificate object
        return cls(hsm, object_id, domain, label, algorithm, certificate, signer_key_id)

    @classmethod
    def generate_intermediate_certificate(cls, hsm, object_id, domain, label, algorithm, signer_key_id, issuer_cert_id, validity=None):
        if validity is None:
            validity = (0, 0)

        # Get the issuer certificate
        issuer_cert = yubihsm.YubiHsmCertificate.get(hsm, issuer_cert_id, domain)

        # Get the public key of the signer key
        signer_key = yubihsm.YubiHsmKey.get(hsm, signer_key_id, domain)
        signer_public_key = signer_key.get_public_key()

        # Build the intermediate certificate
        builder = x509.CertificateBuilder()
        builder = builder.serial_number(x509.random_serial_number())
        builder = builder.issuer_name(issuer_cert.certificate.subject)
        builder = builder.subject_name(x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, label)
        ]))
        builder = builder.not_valid_before(datetime.datetime.utcnow())
        if validity[1] == 0:
            builder = builder.not_valid_after(datetime.datetime.max)
        else:
            builder = builder.not_valid_after(datetime.datetime.utcfromtimestamp(validity[1]))
        builder = builder.public_key(signer_public_key)
        builder = builder.add_extension(x509.KeyUsage(digital_signature=True, key_encipherment=True, key_agreement=True), critical=True)
        builder = builder.add_extension(x509.BasicConstraints(ca=True, path_length=None), critical=True)
        builder = builder.add_extension(x509.AuthorityKeyIdentifier.from_issuer_subject_key_identifier(issuer_cert.certificate.extensions.get_extension_for_oid(x509.OID_SUBJECT_KEY_IDENTIFIER)), critical=False)

        # Sign the certificate
        certificate = builder.sign(
            private_key=signer_key.get_private_key(),
            algorithm=hashes.SHA256(),
            backend=yubihsm.backends.default_backend()
        )

        # Create and return the certificate object
        return cls(hsm, object_id, domain, label, algorithm, certificate, signer_key_id)

    @classmethod
    def generate_cross_signing_certificate(cls, hsm, object_id, domain, label, algorithm, signer_key_id, issuer_cert_id, subject_domain, validity=None):
        if validity is None:
            validity = (0, 0)

        # Get the issuer certificate
        issuer_cert = yubihsm.YubiHsmCertificate.get(hsm, issuer_cert_id, domain)

        # Get the public key of the signer key
        signer_key = yubihsm.YubiHsmKey.get(hsm, signer_key_id, domain)
        signer_public_key = signer_key.get_public_key()

        # Build the cross signing certificate
        builder = x509.CertificateBuilder()
        builder = builder.serial_number(x509.random_serial_number())
        builder = builder.issuer_name(issuer_cert.certificate.subject)
        builder = builder.subject_name(x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, label)
        ]))
        builder = builder.not_valid_before(datetime.datetime.utcnow())
        if validity[1] == 0:
            builder = builder.not_valid_after(datetime.datetime.max)
        else:
            builder = builder.not_valid_after(datetime.datetime.utcfromtimestamp(validity[1]))
        builder = builder.public_key(signer_public_key)
        builder = builder.add_extension(x509.SubjectAlternativeName([x509.DNSName(subject_domain)]), critical=False)
        builder = builder.add_extension(x509.KeyUsage(digital_signature=True, key_encipherment=True, key_agreement=True), critical=True)
        builder = builder.add_extension(x509.BasicConstraints(ca=False, path_length=None), critical=True)

        # Sign the certificate
        certificate = builder.sign(
            private_key=signer_key.get_private_key(),
            algorithm=hashes.SHA256(),
            backend=yubihsm.backends.default_backend()
        )

        # Create and return the certificate object
        return cls(hsm, object_id, domain, label, algorithm, certificate, signer_key_id)

    def put(self):
        """
        Put the certificate on the device.
        """
        self.hsm.put_certificate(
            self.object_id,
            self.domain,
            self.label,
            self.algorithm,
            self.certificate.public_bytes(serialization.Encoding.DER),
            self.signer_key_id
        )

    def delete(self):
        """
        Delete the certificate from the device.
        """
        self.hsm.delete_object(self.object_id)

    def sign_csr(self, csr, subject_key_id, domain=None, validity=None, label=None, algorithm=None):
        """
        Sign a CSR with the specified key and create a certificate on the device.
        """
        if domain is None:
            domain = self.security_domain
        if validity is None:
            validity = (0, 0)
        if label is None:
            label = "Signed Certificate"
        if algorithm is None:
            algorithm = yubihsm.ALGORITHM_RSA_2048

        # Get the subject key
        subject_key = yubihsm.YubiHsmKey.get(self.hsm, subject_key_id, domain)

        # Build the certificate
        builder = x509.CertificateBuilder()
        builder = builder.subject_name(csr.subject)
        builder = builder.issuer_name(csr.subject)
        builder = builder.serial_number(x509.random_serial_number())
        builder = builder.not_valid_before(datetime.datetime.utcnow())
        if validity[1] == 0:
            builder = builder.not_valid_after(datetime.datetime.max)
        else:
            builder = builder.not_valid_after(datetime.datetime.utcfromtimestamp(validity[1]))
        builder = builder.public_key(subject_key.get_public_key())
        builder = builder.add_extension(x509.SubjectKeyIdentifier.from_public_key(subject_key.get_public_key()), critical=False)
        builder = builder.add_extension(x509.BasicConstraints(ca=False, path_length=None), critical=True)

        # Sign the certificate
        certificate = builder.sign(
            private_key=subject_key.get_private_key(),
            algorithm=hashes.SHA256(),
            backend=yubihsm.backends.default_backend()
        )

        # Create the certificate object
        cert = yubihsm.YubiHsmCertificate(
            self.hsm,
            self._get_next_object_id(domain),
            domain,
            label,
            algorithm,
            certificate,
            subject_key_id
        )

        # Put the certificate on the device
        cert.put()

        return cert

    def sign_req(self, req, subject_key_id, domain=None, validity=None, label=None, algorithm=None):
        """
        Sign a REQ with the specified key and create a certificate on the device.
        """
        if domain is None:
            domain = self.security_domain
        if validity is None:
            validity = (0, 0)
        if label is None:
            label = "Signed Certificate"
        if algorithm is None:
            algorithm = yubihsm.ALGORITHM_RSA_2048

        # Get the subject key
        subject_key = yubihsm.YubiHsmKey.get(self.hsm, subject_key_id, domain)

        # Build the certificate
        builder = x509.CertificateBuilder()
        builder = builder.subject_name(req.subject)
        builder = builder.issuer_name(req.subject)
        builder = builder.serial_number(x509.random_serial_number())
        builder = builder.not_valid_before(datetime.datetime.utcnow())
        if validity[1] == 0:
            builder = builder.not_valid_after(datetime.datetime.max)
        else:
            builder = builder.not_valid_after(datetime.datetime.utcfromtimestamp(validity[1]))
        builder = builder.public_key(subject_key.get_public_key())
        builder = builder.add_extension(x509.SubjectKeyIdentifier.from_public_key(subject_key.get_public_key()), critical=False)
        builder = builder.add_extension(x509.BasicConstraints(ca=False, path_length=None), critical=True)

        # Sign the certificate
        certificate = builder.sign(
            private_key=subject_key.get_private_key(),
            algorithm=hashes.SHA256(),
            backend=yubihsm.backends.default_backend()
        )

        # Create the certificate object
        cert = yubihsm.YubiHsmCertificate(
            self.hsm,
            self._get_next_object_id(domain),
            domain,
            label,
            algorithm,
            certificate,
            subject_key_id
        )

        # Put the certificate on the device
        cert.put()

        return cert

    def get_public_key(self):
        """
        Get the public key for the key.
        """
        attrs = self.hsm.get_key_attributes(self.key_id, self.domain)
        pub_key = attrs.public_key
        if self.algorithm == yubihsm.ALGORITHM_RSA_2048:
            return rsa.RSAPublicNumbers(
                e=int.from_bytes(pub_key[:3], 'big'),
                n=int.from_bytes(pub_key[3:], 'big')
            ).public_key(backend=yubihsm.backends.default_backend())
        elif self.algorithm == yubihsm.ALGORITHM_RSA_3072:
            return rsa.RSAPublicNumbers(
                e=int.from_bytes(pub_key[:3], 'big'),
                n=int.from_bytes(pub_key[3:], 'big')
            ).public_key(backend=yubihsm.backends.default_backend())
        elif self.algorithm == yubihsm.ALGORITHM_RSA_4096:
            return rsa.RSAPublicNumbers(
                e=int.from_bytes(pub_key[:3], 'big'),
                n=int.from_bytes(pub_key[3:], 'big')
            ).public_key(backend=yubihsm.backends.default_backend())
        elif self.algorithm == yubihsm.ALGORITHM_EC_P256:
            return ec.EllipticCurvePublicNumbers(
                curve=ec.SECP256R1(),
                x=int.from_bytes(pub_key[:32], 'big'),
                y=int.from_bytes(pub_key[32:], 'big')
            ).public_key(backend=yubihsm.backends.default_backend())
        elif self.algorithm == yubihsm.ALGORITHM_EC_P384:
            return ec.EllipticCurvePublicNumbers(
                curve=ec.SECP384R1(),
                x=int.from_bytes(pub_key[:48], 'big'),
                y=int.from_bytes(pub_key[48:], 'big')
            ).public_key(backend=yubihsm.backends.default_backend())
        elif self.algorithm == yubihsm.ALGORITHM_EC_P521:
            return ec.EllipticCurvePublicNumbers(
                curve=ec.SECP521R1(),
                x=int.from_bytes(pub_key[:66], 'big'),
                y=int.from_bytes(pub_key[66:], 'big')
            ).public_key(backend=yubihsm.backends.default_backend())
        else:
            raise ValueError("Unsupported algorithm")

    def get_pem(self):
        """
        Get the certificate in PEM format.
        """
        attrs = self.hsm.get_certificate_attributes(self.cert_id, self.domain)
        return attrs.certificate.as_pem()

    def get_der(self):
        """
        Get the certificate in DER format.
        """
        attrs = self.hsm.get_certificate_attributes(self.cert_id, self.domain)
        return attrs.certificate.as_der()

    def get_info(self):
        """
        Get information about the certificate.
        """
        attrs = self.hsm.get_certificate_attributes(self.cert_id, self.domain)
        return attrs