from hsm import YubiHSM2

hsm = YubiHSM2("http://127.0.0.1:12345", "password", 1 )

config = {
	"object_id": 0,
	"label": "Test Keypair"
}
#def create_key(self, object_id: int = 0, label: str = None, domain=None, capability=None, algorithm=None):

# pub_key = key.get_public_key()

key = hsm.create_AsymmetricKey(**config)

print(hex(key))

print("HSM Parts: " + str(hsm.get_key_parts()))

print(hsm.get_pub_key())