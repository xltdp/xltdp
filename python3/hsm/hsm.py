# HSM Operations for YubiHSM2

# put authkey 0 2 yubico 1,2,3 generate-asymmetric-key,export-wrapped,get-pseudo-random,put-wrap-key,import-wrapped,delete-asymmetric-key,sign-ecdsa sign-ecdsa,exportable-under-wrap,export-wrapped,import-wrapped <password>

# put authkey 0 2 yubico 1,2,3 get-option,set-option,put-asymmetric-key,generate-asymmetric-key,delete-asymmetric-key
import datetime, re, os
from cryptography.hazmat.primitives import serialization, hashes
from cryptography import x509
from cryptography.x509.oid import NameOID
from cryptography.hazmat.backends import default_backend

from yubihsm import YubiHsm
from yubihsm.objects import AsymmetricKey, ObjectInfo
from yubihsm.defs import ALGORITHM, CAPABILITY

from YubiHsmCertificate import Certificate as YubiHsmCertificate
# from YubiHsmKey import Key as YubiHsmKey

class YubiHSM2:
    def __init__(self, connector=None, password=None, domain=None):
        if domain is None:
            self.domain = 15
        else:
            self.domain = domain
        self.connector = connector
        self.password = password
        self.session = YubiHsm.connect(connector)
        self.hsm = self.session.create_session_derived(self.domain, self.password)

    """
    Validation Decorators
    """

    def AsymmetricKey_validation(func):
        def wrapper(*args, **kwargs):
            if kwargs.get("object_id") is None:
                # Assigns a Random ID so we don't have to.
                object_id = 0
            else:
                object_id = int(kwargs.get("object_id"))

            if kwargs.get("label") is None:
                label = "TEST"
            else:
                label = kwargs.get("label")

            if kwargs.get("domain") is None:
                domain = 15 # self.domain
            else:
                domain = int(kwargs.get("domain"))

            if kwargs.get("algorithm") is None:
                algorithm=ALGORITHM.EC_P384
            else:
                algorithm = kwargs.get("algorithm")

            if kwargs.get("capability") is None:
                if "EC_" in str(algorithm):
                    capability = [ CAPABILITY.SIGN_ECDSA, CAPABILITY.SIGN_PKCS, PUT_ASYMMETRIC ]
                else:
                    capability = [ CAPABILITY.SIGN_PKCS, CAPABILITY.SIGN_PSS, PUT_ASYMMETRIC ]
            else:
                capability = kwargs.get("capability")

            kwargs["object_id"] = object_id
            kwargs["label"] = label
            kwargs["domain"] = domain
            kwargs["algorithm"] = algorithm
            kwargs["capability"] = capability
            return func(*args, **kwargs)
        return wrapper

    def Certificate_validation(func):
        def wrapper(*args, **kwargs):

            # Certificate Profile ( cert_type )
            if kwargs.get("cert_type").__contains__("cross"):
                if kwargs.get("cert_sub_type") is None:
                    sys.exit("Error: Please select 'root' or 'intermediate' for cert_sub_type") 
                if kwargs.get("issuer_certificate_id") is None:
                    sys.exit("Error: Please add the Issuers Certificate ID for issuer_certificate_id") 
                if kwargs.get("issuer_key_id") is None:
                    sys.exit("Error: Please add the Issuers Key ID for issuer_key_id") 
                if kwargs.get("subject_certificate_id") is None:
                    sys.exit("Error: Please add the Subject Certificate ID for subject_certificate_id") 
                if kwargs.get("subject_key_id") is None:
                    sys.exit("Error: Please add the Subject Certificate ID for subject_certificate_id")
            return func(*args, **kwargs)
        return wrapper

    """
    Certificate Decorators
    """

    """
    Create Functions
    """

    @AsymmetricKey_validation
    def create_AsymmetricKey(self, **kwargs):   # ( Tested )
    #def create_key(self, object_id: int = 0, label: str = None, domain=None, capability=None, algorithm=None):

        self.subject_key = AsymmetricKey.generate(
            session=self.hsm,
            object_id=kwargs.get("object_id"),
            label=kwargs.get("label"),
            domains=kwargs.get("domain"),
            capabilities=sum(kwargs.get("capability")),
            algorithm=kwargs.get("algorithm")
        )
        
        return self.subject_key.id

    @Certificate_validation
    def create_Certificate(self, **kwargs):
    #def create_Certificate(self, cert_id=None, domain=None, label=None, algorithm=None, hash_type=None, certificate=None, signer_key_id=None):

    """
    config = {
        "cert_type": "root",
        "cert_sub_type": "",
        "cert_set_label": "My Root Certificate",
        "label": "My Root Certificate Keypair"
        "subject_name": "",
        "subject_certificate_id": ,
        "subject_key_id": ,
        "hash_type": ,
        "not_valid_before": ,
        "not_valid_after": ,

    config = {
        "cert_type": "root",
        "cert_sub_type": "",
        "cert_set_label": "My Root Certificate",
        "label": "My Root Certificate Keypair"
        "issuer_name": "",
        "issuer_certificate_id": ,
        "issuer_key_id": ,
        "subject_name": "",
        "subject_certificate_id": ,
        "subject_key_id": ,
        "hash_type": ,
        "not_valid_before": ,
        "not_valid_after": ,

        ""
    }

    """
        # Certificate KeyPair
        if kwargs.get("subject_key_id") is None:
            # Defaults will already be ECDSA_384
            config = {
                "label": str(re.sub(r'[^a-zA-Z ]+', '', kwargs.get("subject_name").replace(" ", "_"))),
                "domains": self.domains
            }
            # Generate a New Pair of EC Keys
            self.create_AsymmetricKey(config)
        else:
            self.subject_key = self.get_key(kwargs.get("subject_key_id"))
            # Load the Keypair Function

        # Certificate Profile ( cert_type )
        cert_type = kwargs.get("cert_type")

        if cert_type is "root":
            # Certificate Profile Decoration ( Root )
            profile = self.root_profile(**kwargs)
        if cert_type is "issuing":
            # Certificate Profile Decoration ( intermediate )
            profile = self.intermediate_profile(**kwargs)
        if cert_type is "intermediate":
            # Certificate Profile Decoration ( intermediate )
            profile = self.intermediate_profile(**kwargs)
        if cert_type is "cross":
            # Certificate Profile Decoration ( cross )
            profile = self.cross_profile(**kwargs)
        if cert_type is "leaf":
            # Certificate Profile Decoration ( leaf )
            profile = self.leaf_profile(**kwargs)

        # Certificate Signing
        self.certificate = self.sign_certificate(profile, self.key, kwargs.get("hash_type"))

        print(self.set_cert(**kwargs))

    """
    Key Functions
    """

    def get_key_parts(self):

        return self.subject_key

    def get_pub_key(self):
    #def get_key(self, key_id, domain=None):

        return self.subject_key.get_public_key()

    def get_key(self, object_id):

        return AsymmetricKey(object_id=hex(object_id))

    """
    Certificate Functions
    """

    def set_cert(self, **kwargs):

        return AsymmetricKey.put_certificate(kwargs.get("cert_set_label"), self.domain, kwargs.get("capability"), self.certificate)


    def _cert_data(self,**kwargs)

        builder = x509.CertificateBuilder()

        builder = builder.serial_number(x509.random_serial_number())

        builder = builder.subject_name(x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, kwargs.get("subject_name")),
        ]))
        builder = builder.issuer_name(x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, kwargs.get("issuer_name")),
        ]))

        builder = builder.not_valid_before(kwargs.get("not_valid_before"))
        builder = builder.not_valid_after(kwargs.get("not_valid_after"))

        profile = profile.public_key(self.subject_key.get_public_key())

        builder = builder.add_extension(
            x509.SubjectAlternativeName(
                [ x509.DNSName( kwargs.get("subject_name") ) ]
            ),
            critical=False
        )

        return builder

    def sign_certificate(data, key, algorithm):

        if str(algorithm).__contains__("EC"):
            certificate = key.sign_ecdsa(data)
        else:
            certificate = key.sign_pkcs1v1_5(data)

        return certificate

    def root_profile(self, **kwargs):

        # Generic Certificate Data ( All Certs will have )
        profile = self._cert_data(**kwargs)

        # Root Specific Certificate Data ( root certs )
        profile = profile.add_extension(
            x509.BasicConstraints(ca=True, path_length=None), critical=True,
        )

        return profile

    def intermediate_profile(self, **kwargs):

        # Generic Certificate Data ( All Certs will have )
        profile = self._cert_data(**kwargs)

        # Intermediate Specific Certificate Data ( Intermediate/Issuing certs )
        profile = profile.add_extension(
            x509.BasicConstraints(ca=True, path_length=1), critical=True,
        )

        return profile

    def cross_profile(self, **kwargs):

        # Generic Certificate Data ( All Certs will have )
        profile = self._cert_data(**kwargs)

        # Cross Specific Certificate Data ( Cross certs )

        if kwargs.get("cert_sub_type") is "root":
            profile = profile.add_extension(
                x509.BasicConstraints(ca=True, path_length=None), critical=True,
            )
        else:
            profile = profile.add_extension(
                x509.BasicConstraints(ca=True, path_length=1), critical=True,
            )

        profile = profile.add_extension( 
            x509.SubjectKeyIdentifier.from_public_key( subject_cert.public_key() ),
            critical=False
        )

        profile = profile.add_extension( 
            x509.AuthorityKeyIdentifier.from_issuer_public_key( issuer_cert.public_key() ),
            critical=False
        )

        return profile





#####

    def delete_key(self, key_id, domain=None):
        if domain is None:
            domain = self.security_domain

        key = YubiHsmKey.get(self.hsm, key_id, domain)
        key.delete()

    def update_key(self, key_id=None, domain=None, label=None, capability=None, exportable=False):
        if capability is None:
            capability = []
        """
        Update a key on the YubiHSM 2 device.
        """
        # Get the existing key
        key = self.get_key(key_id, domain)

        # Update the label and exportability of the key
        key.label = label
        key.capability = capability
        key.exportable = exportable

        # Put the updated key back on the device
        key.put()

    def create_certificate(self, cert_id=None, domain=None, label=None, algorithm=None, certificate=None, signer_key_id=None):
        if domain is None:
            domain = self.security_domain

        cert = YubiHsmCertificate(
            self.hsm,
            cert_id,
            domain,
            label,
            algorithm,
            certificate,
            signer_key_id
        )
        cert.put()

    def create_root_certificate(self, root_cert_id=None, domain=None, label=None, algorithm=None, signer_key_id=None):
        """
        Create a root certificate in the specified domain.
        """
        # Generate the root certificate
        root_cert = YubiHsmCertificate.generate_root_certificate(
            self.hsm,
            root_cert_id,
            domain,
            label,
            algorithm,
            signer_key_id
        )

        # Put the root certificate on the device
        root_cert.put()

    def create_intermediate_certificate(self, intermediate_cert_id=None, domain=None, label=None, algorithm=None, signer_key_id=None, issuer_cert_id=None):
        """
        Create an intermediate certificate in the specified domain.
        """
        # Generate the intermediate certificate
        intermediate_cert = YubiHsmCertificate.generate_intermediate_certificate(
            self.hsm,
            intermediate_cert_id,
            domain,
            label,
            algorithm,
            signer_key_id,
            issuer_cert_id
        )

        # Put the intermediate certificate on the device
        intermediate_cert.put()

    def create_cross_signing_certificate(self, cross_signing_cert_id=None, domain=None, label=None, algorithm=None, signer_key_id=None, issuer_cert_id=None, subject_domain=None):
        """
        Create a cross signing certificate in the specified domain.
        """
        # Generate the cross signing certificate
        cross_signing_cert = YubiHsmCertificate.generate_cross_signing_certificate(
            self.hsm,
            cross_signing_cert_id,
            domain,
            label,
            algorithm,
            signer_key_id,
            issuer_cert_id,
            subject_domain
        )

        # Put the cross signing certificate on the device
        cross_signing_cert.put()


    def get_certificate(self, cert_id=None, domain=None):
        if domain is None:
            domain = self.security_domain

        cert = YubiHsmCertificate.get(self.hsm, cert_id, domain)
        return cert

    def delete_certificate(self, cert_id=None, domain=None):
        if domain is None:
            domain = self.security_domain

        cert = YubiHsmCertificate.get(self.hsm, cert_id, domain)
        cert.delete()

    def update_certificate(self, cert_id=None, domain=None, label=None, certificate=None):
        if domain is None:
            domain = self.security_domain
        """
        Update a certificate on the YubiHSM 2 device.
        """
        # Get the existing certificate
        cert = self.get_certificate(cert_id, domain)

        # Update the label and certificate of the certificate
        cert.label = label
        cert.certificate = certificate

        # Put the updated certificate back on the device
        cert.put()

    def verify_key(self, key_id=None, domain=None, message=None, signature=None):
        if domain is None:
            domain = self.security_domain
        """
        Verify a signature using a key.
        """
        key = self.get_key(key_id, domain)
        return key.verify(message, signature)

    def verify_certificate(self, cert_id=None, domain=None, message=None, signature=None):
        if domain is None:
            domain = self.security_domain
        """
        Verify a signature using a certificate.
        """
        cert = self.get_certificate(cert_id, domain)
        return cert.verify(message, signature)

    def list_items(self, domain=None):
        if domain is None:
            domain = self.security_domain
        """
        List all keys, certificates, and other items in the specified domain.
        """
        items = []

        # Get a list of all keys in the domain
        keys = YubiHsmKey.get_all(self.hsm, domain)
        items.extend(keys)

        # Get a list of all certificates in the domain
        certificates = YubiHsmCertificate.get_all(self.hsm, domain)
        items.extend(certificates)

        # Return the list of items
        return items

    def sign_csr(self, key_id=None, csr=None, domain=None):
        if domain is None:
            domain = self.security_domain

        """
        Signs a CSR 
        """

        # Get the key object
        key = YubiHsmKey.get(self.hsm, key_id, domain)

        # Sign the CSR
        certificate = key.sign_csr(csr)

        # Return the Signed Certificate
        return certificate