from hsm2 import YubiHSM2

hsm = YubiHSM2("http://127.0.0.1:12345", "password", "1" )

# Generate a 4096-bit RSA key pair on the HSM
key_id = hsm.create_rsa_key(4096)

# Create a self-signed certificate
certificate = hsm.create_self_signed_certificate(key_id, 'CN=My Certificate',
                                                  algorithm=yubihsm.SIGN_ALGORITHM_RSASSA_PKCS1_PSS_SHA256)

# Sign the certificate with the private key
signature = hsm.sign(key_id, certificate, yubihsm.SIGN_ALGORITHM_RSASSA_PKCS1_PSS_SHA256)

