import logging
import struct

from collections import namedtuple

# from yubihsm.connector import YubiHsmConnector
from yubihsm.objects import *
from yubihsm.defs import *
from yubihsm.utils import *

class Key:
    """
    Class representing a key on a YubiHSM 2 device.
    """
    def __init__(self, hsm, key_id, domain, algorithm, label, capability, exportable):
        """
        Initialize a new YubiHsmKey object.
        """
        self.hsm = hsm
        self.key_id = key_id
        self.domain = domain
        self.algorithm = algorithm
        self.label = label
        self.capability = capability
        self.exportable = exportable

    def put(self):
        """
        Put the key on the YubiHSM 2 device.
        """
        self.hsm.AsymmetricKey.put(self.key_id, self.domain, self.algorithm, self.label, self.capability, self.exportable)

    @classmethod
    def get(cls, hsm, key_id, domain):
        """
        Get a key from the YubiHSM 2 device.
        """
        attrs = hsm.get_key_attributes(key_id, domain)
        return cls(hsm, key_id, domain, attrs.algorithm, attrs.label, attrs.capability, attrs.exportable)

    def delete(self):
        """
        Delete the key from the YubiHSM 2 device.
        """
        self.hsm.delete_key(self.key_id, self.domain)

    def sign(self, algorithm, data):
        """
        Sign data using the key.
        """
        return self.hsm.sign(self.key_id, algorithm, data)

    def verify(self, algorithm, data, signature):
        """
        Verify a signature using the key.
        """
        return self.hsm.verify(self.key_id, algorithm, data, signature)

    def wrap(self, algorithm, data):
        """
        Wrap data using the key.
        """
        return self.hsm.wrap(self.key_id, algorithm, data)

    def unwrap(self, algorithm, data, iv):
        """
        Unwrap data using the key.
        """
        return self.hsm.unwrap(self.key_id, algorithm, data, iv)

    def export(self, password):
        """
        Export the key in encrypted form.
        """
        return self.hsm.export_wrapped(self.key_id, self.domain, password)

    def generate_hmac(self, algorithm, data):
        """
        Generate an HMAC using the key.
        """
        return self.hsm.generate_hmac(self.key_id, algorithm, data)

    def verify_hmac(self, algorithm, data, hmac):
        """
        Verify an HMAC using the key.
        """
        return self.hsm.verify_hmac(self.key_id, algorithm, data, hmac)