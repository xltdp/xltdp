# hsm/YubiHSM2 class
Usage examples for the methods in the YubiHSM2 class
  
## 1. Create a new key:  
```
hsm = YubiHSM2(connector, password, security_domain)
hsm.create_key(1, yubihsm.ALGORITHM_EC_P256, "my key", [yubihsm.CAPABILITY_SIGN_ECDSA])
```
This will create a new key with the ID `1`, using the EC P256 algorithm and the label "my key", and with the capability to sign using ECDSA, in the specified `security_domain`.
  
## 2. Get an existing key:  
```
hsm = YubiHSM2(connector, password, security_domain)
key = hsm.get_key(1)
print(key.label)  # prints "my key"
```
This will retrieve the key with the ID `1` from the HSM, and print its label.
  
## 3. Update an existing key:  
```
hsm = YubiHSM2(connector, password, security_domain)
hsm.update_key(1, label="updated key", capability=[yubihsm.CAPABILITY_SIGN_HMAC])
```
This will update the key with the ID `1`, setting its label to "updated key" and its capability to sign using HMAC.
  
## 4. Delete an existing key:  
```
hsm = YubiHSM2(connector, password, security_domain)
hsm.delete_key(1)
```
This will delete the key with the ID `1` from the HSM.
  
## 5. Create a certificate:  
```
hsm = YubiHSM2(connector, password, security_domain)
certificate = "-----BEGIN CERTIFICATE-----\n...\n-----END CERTIFICATE-----"
hsm.create_certificate(1, "my certificate", yubihsm.ALGORITHM_EC_P256, certificate, 1)
```
This will create a new certificate with the ID `1`, using the label "my certificate", the EC P256 algorithm, and the provided certificate data, and signed using the key with the ID `1`.
  
## 6. Create a root certificate:  
```
hsm = YubiHSM2(connector, password, security_domain)
hsm.create_root_certificate(1, "my root certificate", yubihsm.ALGORITHM_EC_P256, 1)
```
This will create a new root certificate with the ID `1`, using the label "my root certificate", the EC P256 algorithm, and signed using the key with the ID `1`.

## 7. Create an intermediate certificate:
```
hsm = YubiHSM2(connector, password, security_domain)
hsm.create_intermediate_certificate(1, "my intermediate certificate", yubihsm.ALGORITHM_EC_P256, 1, 2)
```
This will create a new intermediate certificate with the ID `1`, using the label "my intermediate certificate", the EC P256 algorithm, signed using the key with the ID 1, and issued by the certificate with the ID `2`.

## 8. Create an cross certificate:
```
hsm = YubiHSM2(connector, password, security_domain)
hsm.create_cross_signing_certificate(1, "my cross signing certificate", yubihsm.ALGORITHM_EC_P256, 1, 2, 3)
```
This will create a new cross signing certificate with the ID `1`, using the label "my cross signing certificate", the EC P256 algorithm, signed using the key with the ID `1`, issued by the certificate with the ID `2`, and with the subject domain `3`.

## 9. Create a root certificate with params [ valid_from, valid_to ]
When creating a root certificate using YubiHSM2.create_root_certificate(), you can specify the start and end dates of the certificate by passing the valid_from and valid_to arguments, respectively. Both arguments should be Unix timestamps (the number of seconds since 1970-01-01 00:00:00 UTC).

Here is an example of how you can create a root certificate with start and end dates:
```
import time

# Get the current time as a Unix timestamp
now = int(time.time())

# Calculate the Unix timestamps for one year from now
one_year_from_now = now + (3600 * 24 * 365)

hsm = YubiHSM2(connector, password, security_domain)
hsm.create_root_certificate(1, "my root certificate", yubihsm.ALGORITHM_EC_P256, 1, valid_from=now, valid_to=one_year_from_now)
```
This will create a new root certificate with the ID `1`, using the label "my root certificate", the EC P256 algorithm, signed using the key with the ID `1`, and valid from the current time until one year from now.

## 10. Create a Cross Signed Root
To create a cross signed root certificate, you will need to create two root certificates: one that will be the issuer, and another that will be the subject. You will then use the YubiHSM2.create_cross_signing_certificate() method to create the cross signing certificate, which will act as a bridge between the two root certificates.

Here is an example of how you can create a cross signed root:

```
# Connect to the HSM
hsm = YubiHSM2(connector, password, security_domain)

# Create the issuer root certificate
hsm.create_root_certificate(1, "issuer root", yubihsm.ALGORITHM_EC_P256, 1)

# Create the subject root certificate
hsm.create_root_certificate(2, "subject root", yubihsm.ALGORITHM_EC_P256, 2)

# Create the cross signing certificate
hsm.create_cross_signing_certificate(3, "cross signing certificate", yubihsm.ALGORITHM_EC_P256, 1, 1, 2)
```
This will create two root certificates: one with the ID `1`, label "issuer root", and signed using the key with the ID `1`, and another with the ID `2`, label "subject root", and signed using the key with the ID `2`. It will then create a cross signing certificate with the ID `3`, label "cross signing certificate", and signed using the key with the ID `1`, issued by the issuer root certificate with the ID `1`, and with the subject root certificate with the ID `2`. 

## 11. Create Cross Signed Root
To create a cross signed intermediate certificate, you will need to create an issuer root certificate, a subject root certificate, an intermediate certificate that will act as the issuer, and another intermediate certificate that will be the subject. You will then use the YubiHSM2.create_cross_signing_certificate() method to create the cross signing certificate, which will act as a bridge between the two intermediate certificates.

Here is an example of how you can create a cross signed intermediate certificate:

```
# Connect to the HSM
hsm = YubiHSM2(connector, password, security_domain)

# Create the issuer root certificate
hsm.create_root_certificate(1, "issuer root", yubihsm.ALGORITHM_EC_P256, 1)

# Create the subject root certificate
hsm.create_root_certificate(2, "subject root", yubihsm.ALGORITHM_EC_P256, 2)

# Create the issuer intermediate certificate
hsm.create_intermediate_certificate(3, "issuer intermediate", yubihsm.ALGORITHM_EC_P256, 1, 1)

# Create the subject intermediate certificate
hsm.create_intermediate_certificate(4, "subject intermediate", yubihsm.ALGORITHM_EC_P256, 2, 2)

# Create the cross signing certificate
hsm.create_cross_signing_certificate(5, "cross signing certificate", yubihsm.ALGORITHM_EC_P256, 1, 3, 4)
```
This will create two root certificates: one with the ID `1`, label "issuer root", and signed using the key with the ID `1`, and another with the ID `2`, label "subject root", and signed using the key with the ID `2`. It will then create two intermediate certificates: one with the ID `3`, label "issuer intermediate", signed using the key with the ID `1`, and issued by the issuer root certificate with the ID `1`, and another with the ID `4`, label "subject intermediate", signed using the key with the ID `2`, and issued by the subject root certificate with the ID `2`. Finally, it will create a cross signing certificate with the ID `5`, label "cross signing certificate", and signed using the key with the ID `1`, issued by the issuer intermediate certificate with the ID `3`, and with the subject intermediate certificate with the ID `4`.

To create a leaf certificate using an intermediate cross signing certificate as the issuer, you can use the YubiHSM2.create_certificate() method, passing the ID of the intermediate cross signing certificate as the signer_key_id argument.

Here is an example of how you can create a leaf certificate using an intermediate cross signing certificate as the issuer:

```
# Connect to the HSM
hsm = YubiHSM2(connector, password, security_domain)

# Create the issuer root certificate
hsm.create_root_certificate(1, "issuer root", yubihsm.ALGORITHM_EC_P256, 1)

# Create the subject root certificate
hsm.create_root_certificate(2, "subject root", yubihsm.ALGORITHM_EC_P256, 2)

# Create the issuer intermediate certificate
hsm.create_intermediate_certificate(3, "issuer intermediate", yubihsm.ALGORITHM_EC_P256, 1, 1)

# Create the subject intermediate certificate
hsm.create_intermediate_certificate(4, "subject intermediate", yubihsm.ALGORITHM_EC_P256, 2, 2)

# Create the cross signing certificate
hsm.create_cross_signing_certificate(5, "cross signing certificate", yubihsm.ALGORITHM_EC_P256, 1, 3, 4)

# Create a leaf certificate
certificate = "-----BEGIN CERTIFICATE-----\n...\n-----END CERTIFICATE-----"

hsm.create_certificate(6, "leaf certificate", yubihsm.ALGORITHM_EC_P256, certificate, 5)
```
This will create a leaf certificate with the ID 6, label "leaf certificate", and signed using the intermediate cross signing certificate with the ID 5 as the issuer.

## 12. Sign a CSR with a Cross signed Certificate ( ECDSA 256 )
To sign a Certificate Signing Request (CSR) using an intermediate cross signing certificate, you can use the YubiHSM2.sign_csr() method, passing the ID of the intermediate cross signing certificate as the key_id argument and the CSR as the csr argument.

Here is an example of how you can sign a CSR using an intermediate cross signing certificate:

```
# Connect to the HSM
hsm = YubiHSM2(connector, password, security_domain)

# Create the issuer root certificate
hsm.create_root_certificate(1, "issuer root", yubihsm.ALGORITHM_EC_P256, 1)

# Create the subject root certificate
hsm.create_root_certificate(2, "subject root", yubihsm.ALGORITHM_EC_P256, 2)

# Create the issuer intermediate certificate
hsm.create_intermediate_certificate(3, "issuer intermediate", yubihsm.ALGORITHM_EC_P256, 1, 1)

# Create the subject intermediate certificate
hsm.create_intermediate_certificate(4, "subject intermediate", yubihsm.ALGORITHM_EC_P256, 2, 2)

# Create the cross signing certificate
hsm.create_cross_signing_certificate(5, "cross signing certificate", yubihsm.ALGORITHM_EC_P256, 1, 3, 4)

# Sign the CSR using the intermediate cross signing certificate
csr = "-----BEGIN CERTIFICATE REQUEST-----\n...\n-----END CERTIFICATE REQUEST-----"
certificate = hsm.sign_csr(5, csr)
```
This will sign the CSR using the intermediate cross signing certificate with the ID 5 as the issuer, and return the signed certificate as a string.

## 13. Sign a CSR with a Cross signed Certificate ( RSA 4096 )
To sign a Certificate Signing Request (CSR) using an intermediate cross signing certificate and the RSA 4096 algorithm, you can use the YubiHSM2.sign_csr() method, passing the ID of the intermediate cross signing certificate as the key_id argument and the CSR as the csr argument.

Here is an example of how you can sign a CSR using an intermediate cross signing certificate and the RSA 4096 algorithm:

```
# Connect to the HSM
hsm = YubiHSM2(connector, password, security_domain)

# Create the issuer root certificate
hsm.create_root_certificate(1, "issuer root", yubihsm.ALGORITHM_RSA_4096, 1)

# Create the subject root certificate
hsm.create_root_certificate(2, "subject root", yubihsm.ALGORITHM_RSA_4096, 2)

# Create the issuer intermediate certificate
hsm.create_intermediate_certificate(3, "issuer intermediate", yubihsm.ALGORITHM_RSA_4096, 1, 1)

# Create the subject intermediate certificate
hsm.create_intermediate_certificate(4, "subject intermediate", yubihsm.ALGORITHM_RSA_4096, 2, 2)

# Create the cross signing certificate
hsm.create_cross_signing_certificate(5, "cross signing certificate", yubihsm.ALGORITHM_RSA_4096, 1, 3, 4)

# Sign the CSR using the intermediate cross signing certificate
csr = "-----BEGIN CERTIFICATE REQUEST-----\n...\n-----END CERTIFICATE REQUEST-----"
certificate = hsm.sign_csr(5, csr)
```
This will sign the CSR using the intermediate cross signing certificate with the ID 5 as the issuer, and return the signed certificate as a string.