import yubihsm
from cryptography.x509.name import Name
from cryptography.x509.certificate_request import CertificateRequest
from cryptography.x509.oid import ExtensionOID
from cryptography.hazmat.primitives.serialization import Encoding
from datetime import datetime, timedelta
from time import time

class YubiHSM2:
    def __init__(self, connector=None, password=None, security_domain=None):
        if security_domain is None:
            self.security_domain = 15
        else:
            self.security_domain = security_domain
        self.connector = connector
        self.password = password

        self.session = YubiHsm.connect(self.connector)
        self._session = self.session.create_session_derived(self.security_domain, self.password)
        self.session_counter = 0

    def create_rsa_key(self, bits):
        return self._create_key(KeyType.RSA, bits=bits)

    def create_ec_key(self, curve):
        return self._create_key(KeyType.EC, curve=curve)

    def import_key(self, key_type, id, domain, capability, algorithm, public_key, private_key=None):
        self._send(Command.PUT_ASYMMETRIC_KEY, key_type, id, domain, capability, algorithm, public_key,
                   private_key=private_key)

    def delete_object(self, object_id):
        self._send(Command.DELETE_OBJECT, object_id)

    def send_certificate_request(self, certificate_request):
        self._send(Command.PUT_CERTIFICATE_REQUEST, certificate_request.public_bytes(Encoding.DER))

    def sign_certificate(self, certificate, key_id, algorithm):
        certificate_bytes = certificate.public_bytes(Encoding.DER)
        self._sign(key_id, certificate_bytes, algorithm)
        self._send(Command.PUT_CERTIFICATE, certificate_bytes)

    def create_self_signed_certificate(self, key_id, subject, not_before=None, not_after=None, algorithm=None, curve=None, serial_number=None):
        """
        Create a self-signed X.509 certificate using the specified key and subject.
        """
        if not_before is None:
            not_before = datetime.utcnow()
        if not_after is None:
            not_after = not_before + timedelta(days=3650)
        if serial_number is None:
            serial_number = int(time())

        certificate_request = CertificateSigningRequestBuilder() 
        # certificate_request = CertificateRequest()
        certificate_request.subject = Name.from_text(subject)
        certificate_request.not_valid_before = not_before
        certificate_request.not_valid_after = not_after
        certificate_request.serial_number = serial_number

        key_type, key_algorithm, curve_name = self._get_key_info(key_id)

        if algorithm is None:
            if key_type == KeyType.EC:
                algorithm = self._get_ecdsa_algorithm(curve_name)
            elif key_type == KeyType.RSA:
                algorithm = self._get_rsa_algorithm()
            else:
                raise ValueError("Unsupported key type")

        if key_type == KeyType.EC:
            public_key = self.get_ec_public_key(key_id)
        elif key_type == KeyType.RSA:
            public_key = self.get_rsa_public_key(key_id)
        else:
            raise ValueError("Unsupported key type")

        certificate_request.public_key = public_key
        certificate_request.sign(private_key=public_key, algorithm=algorithm)

        certificate = certificate_request.certificate
        self.send_certificate_request(certificate_request)
        self.sign_certificate(certificate, key_id, algorithm)

        return certificate