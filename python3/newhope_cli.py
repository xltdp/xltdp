import argparse
import getpass
import sys

from newhope_cipher import NewHopeCipher

def process_input(cipher, data, ciphertext, salt, verify):
    if verify:
        # Verify the data and ciphertext
        result = cipher.verify(data, ciphertext, salt)
        if result:
            print('Verification succeeded')
        else:
            print('Verification failed', file=sys.stderr)
    else:
        # Check if the ciphertext argument was provided
        if ciphertext:
            # Decrypt the data
            data = cipher.decrypt(ciphertext, salt)
            if data:
                print(f'Decrypted data: {data}')
                # Write the decrypted data to a file
                with open('decrypted.txt', 'wb') as f:
                    f.write(data)
        else:
            # Encrypt the data
            ciphertext = cipher.encrypt(data, salt)
            print(f'Encrypted data: {ciphertext}')

def main():
    # Parse the command-line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--password', required=True, help='the password to use for encryption and decryption')
    parser.add_argument('-s', '--salt', required=True, help='the salt to use for key derivation')
    parser.add_argument('-d', '--data', required=False, help='the data to be encrypted or decrypted')
    parser.add_argument('-c', '--ciphertext', required=False, help='the ciphertext to be decrypted or verified')
    parser.add_argument('-f', '--file', required=False, help='the file to be encrypted or decrypted')
    parser.add_argument('-v', '--verify', action='store_true', help='verify the data and ciphertext')
    args = parser.parse_args()

    # Create a NewHopeCipher object with the password and salt
    cipher = NewHopeCipher(args.password, args.salt)

    if args.file:
        # Read the file
        with open(args.file, 'rb') as f:
            data = f.read()
        # Process the input
        process_input(cipher, data, args.ciphertext, args.salt, args.verify)
    elif args.data:
        # Process the input
        process_input(cipher, args.data.encode(), args.ciphertext, args.salt, args.verify)
    else:
        print('Error: either the data or file argument must be provided', file=sys.stderr)

if __name__ == '__main__':
    main()