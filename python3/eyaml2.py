import base64
import rsa
import yaml

class Eyaml:
    def __init__(self, private_key_file=None, public_key_file=None):
        if private_key_file:
            with open(private_key_file, 'rb') as f:
                self.private_key = rsa.PrivateKey.load_pkcs1(f.read())
        if public_key_file:
            with open(public_key_file, 'rb') as f:
                self.public_key = rsa.PublicKey.load_pkcs1(f.read())

    def generate_key_pair(self, private_key_file, public_key_file):
        (self.private_key, self.public_key) = rsa.newkeys(512)
        with open(private_key_file, 'wb') as f:
            f.write(self.private_key.save_pkcs1())
        with open(public_key_file, 'wb') as f:
            f.write(self.public_key.save_pkcs1())

    def encrypt(self, data):
        return base64.b64encode(rsa.encrypt(data.encode(), self.public_key)).decode()

    def decrypt(self, data):
        return rsa.decrypt(base64.b64decode(data), self.private_key).decode()

    def encrypt_yaml(self, yaml_file, encrypted_yaml_file):
        with open(yaml_file, 'r') as f:
            data = yaml.safe_load(f)

        encrypted_data = {}
        for key, value in data.items():
            encrypted_key = self.encrypt(key)
            encrypted_value = self.encrypt(value)
            encrypted_data[encrypted_key] = encrypted_value

        with open(encrypted_yaml_file, 'w') as f:
            yaml.safe_dump(encrypted_data, f)

    def decrypt_eyaml(self, encrypted_yaml_file, decrypted_yaml_file):
        with open(encrypted_yaml_file, 'r') as f:
            encrypted_data = yaml.safe_load(f)

        decrypted_data = {}
        for encrypted_key, encrypted_value in encrypted_data.items():
            key = self.decrypt(encrypted_key)
            value = self.decrypt(encrypted_value)
            decrypted_data[key] = value

        with open(decrypted_yaml_file, 'w') as f:
            yaml.safe_dump(decrypted_data, f)

    def add_entry(self, yaml_file, key, value):
        with open(yaml_file, 'r') as f:
            data = yaml.safe_load(f)

        data[key] = value

        with open(yaml_file, 'w') as f:
            yaml.safe_dump(data, f)