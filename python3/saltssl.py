import hashlib

class SaltSSL:
    def __init__(self, certificate_path):
        self.certificate_path = certificate_path

    def compute_thumbprint(self, hash_function='sha1'):
        with open(self.certificate_path, 'rb') as f:
            data = f.read()
            hash_obj = hashlib.new(hash_function)
            hash_obj.update(data)
            return hash_obj.hexdigest()