# Import the necessary libraries
from google.oauth2.credentials import Credentials
import google.auth
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# Import the PostQuantumCiphers class
from postquantum_ciphers import PostQuantumCiphers

class PQCGDriveOps:
    def __init__(self, password, salt, algorithm):
        # Create a PostQuantumCiphers object with the password, salt, and algorithm
        self.cipher = PostQuantumCiphers(password, salt, algorithm)

        # Set up the Google Drive API client
        try:
            # Get the application default credentials
            credentials, project = google.auth.default()

            # Build the Google Drive API client
            self.service = build('drive', 'v3', credentials=credentials)
        except Exception as e:
            print(f'Error setting up Google Drive API client: {e}')

    def _download_file(self, file_id):
        # Download the file from Google Drive
        try:
            file = self.service.files().get(fileId=file_id).execute()
            file_content = self.service.files().get_media(fileId=file_id).execute()
        except HttpError as error:
            print(f'Error downloading file from Google Drive: {error}')
            return None

        # Read the contents of the file
        return file_content.decode('utf-8')

    def _upload_file(self, file_id, file_name, data):
        # Upload the data to Google Drive
        try:
            file_metadata = {'name': file_name}
            media = googleapiclient.http.MediaIoBaseUpload(io.BytesIO(data), mimetype='application/octet-stream', chunksize=1024*1024, resumable=True)
            file = self.service.files().create(body=file_metadata, media_body=media, fields='id').execute()
        except HttpError as error:
            print(f'Error uploading file to Google Drive: {error}')
            return False

        return True

    def encrypt_file(self, input_file, output_file, salt):
        # Check if the input file is on Google Drive
        if input_file.startswith('gdrv:'):
            # Get the file ID from the input file path
            file_id = input_file[5:]

            # Download the file from Google Drive
            data = self._download_file(file_id)
            if data is None:
                return
        else:
            # Read the contents of the file from the local file system
            with open(input_file, 'r') as f:
                data = f.read()

        # Encrypt the data
        ciphertext = self.cipher.encrypt(data, salt)

        # Check if the output file is on Google Drive
        if output_file.startswith('gdrv:'):
            # Get the file ID and name from the output file path
            file_id = output_file[5:]
            file_name = file_id.split('/')[-1]

            # Upload the ciphertext to Google Drive
            if not self._upload_file(file_id, file_name, ciphertext):
                return
        else:
            # Write the ciphertext to a file on the local file system
            with open(output_file, 'wb') as f:
                f.write(ciphertext)


    def decrypt_file(self, input_file, output_file, salt):
        # Check if the input file is on Google Drive
        if input_file.startswith('gdrv:'):
            # Get the file ID from the input file path
            file_id = input_file[5:]

            # Download the file from Google Drive
            ciphertext = self._download_file(file_id)
            if ciphertext is None:
                return
        else:
            # Read the ciphertext from the local file system
            with open(input_file, 'rb') as f:
                ciphertext = f.read()

        # Decrypt the ciphertext
        data = self.cipher.decrypt(ciphertext, salt)

        # Check if the output file is on Google Drive
        if output_file.startswith('gdrv:'):
            # Get the file ID and name from the output file path
            file_id = output_file[5:]
            file_name = file_id.split('/')[-1]

            # Upload the data to Google Drive
            if not self._upload_file(file_id, file_name, data):
                return
        else:
            # Write the data to a file on the local file system
            with open(output_file, 'w') as f:
                f.write(data)