# Generate a new RSA key pair
private_key, public_key = EFHAMDLE.generate_rsa_key_pair()

# Save the key pair to files
EFHAMDLE.save_rsa_key_pair(private_key, public_key, "private_key.pem", "public_key.pem")

# Load the key pair from files
private_key = EFHAMDLE.load_rsa_private_key("private_key.pem")
public_key = EFHAMDLE.load_rsa_public_key("public_key.pem")

# Initialize the EFHAMDLE class with the key pair
efhamdle = EFHAMDLE(private_key=private_key, public_key=public_key)

# Encrypt a string and write it to a file
efhamdle.encrypt_string_to_file("Hello, World!", "encrypted.bin")

# Decrypt the string from the file
decrypted_string = efhamdle.decrypt_string_from_file("encrypted.bin")
print(decrypted_string)  # prints "Hello, World!"

# Sign some data
data = b"This is the data to be signed."
signature = efhamdle.sign_data(data)

# Verify the signature
is_signature_valid = efhamdle.verify_signature(data, signature)
print(is_signature_valid)  # prints True

# Try to verify the signature with the wrong data
is_signature_valid = efhamdle.verify_signature(b"This is the wrong data.", signature)
print(is_signature_valid)  # prints False