import os, logging

# Import the KeyGeneration, KeyStorage, Cryptography, and Authentication classes
from keygeneration import KeyGeneration
from keystorage import KeyStorage
from cryptography import Cryptography
from authentication import Authentication

class KSM:
    def __init__(self):
        self.key_gen = KeyGeneration()
        self.key_store = KeyStorage()
        self.crypto = Cryptography()
        self.auth = Authentication()
        self.logger = logging.getLogger(__name__)
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')

    def generate_key_pair(self, key_size=2048):
        self.logger.info("Generating key pair with key size %s", key_size)
        self.key_gen.generate_key_pair(key_size)
        
    def export_key_pair(self, private_key_file, public_key_file, passphrase=None):
        self.logger.info("Exporting key pair to files %s, %s", private_key_file, public_key_file)
        self.key_gen.export_private_key(passphrase, private_key_file)
        self.key_gen.export_public_key(public_key_file)

    def import_key_pair(self, private_key_file, public_key_file, passphrase=None):
        self.logger.info("Importing key pair from files %s, %s", private_key_file, public_key_file)
        self.key_store.import_private_key(passphrase, private_key_file)
        self.key_store.import_public_key(public_key_file)
        self.crypto.set_keys(self.key_store.private_key, self.key_store.public_key)

    def encrypt(self, data, username, password):
        self.logger.info("User %s is trying to encrypt the data", username)
        if self.auth.authenticate(username, password):
            self.logger.info("User %s is authenticated, encrypting the data", username)
            return self.crypto.encrypt(data)
        else:
            self.logger.error("Invalid username or password for user %s", username)
            raise ValueError("Invalid username or password")
    
    def decrypt(self, ciphertext, username, password):
        self.logger.info("User %s is trying to decrypt the data", username)
        if self.auth.authenticate(username, password):
            self.logger.info("User %s is authenticated, decrypting the data", username)
            return self.crypto.decrypt(ciphertext)
        else:
            self.logger.error("Invalid username or password for user %s", username)
            raise ValueError("Invalid username or password")