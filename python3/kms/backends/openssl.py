from cryptography.hazmat.primitives import padding
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives import hashes

class Cryptography:
    def __init__(self):
        self.private_key = None
        self.public_key = None

    def set_keys(self, private_key, public_key):
        self.private_key = private_key
        self.public_key = public_key

    def encrypt(self, data):
        if self.public_key:
            ciphertext = self.public_key.encrypt(
                data,
                padding.OAEP(
                    mgf=padding.MGF1(algorithm=hashes.SHA512_256()),
                    algorithm=hashes.SHA256(),
                    label=None
                )
            )
            return ciphertext
        else:
            raise ValueError("No public key found!")
    
    def decrypt(self, ciphertext):
        if self.private_key:
            data = self.private_key.decrypt(
                ciphertext,
                padding.OAEP(
                    mgf=padding.MGF1(algorithm=hashes.SHA256()),
                    algorithm=hashes.SHA256(),
                    label=None
                )
            )
            return data
        else:
            raise ValueError("No private key found!")
