from cryptography.hazmat.primitives.kdf.scrypt import Scrypt

class KeyVerification:

    def __init__(self):
        pass

    def verify_key(self, salt, key_material, expected_key):
        """
        Verify that a key is correct
        :param key_material: bytes, the password to check
        :param expected_key: bytes, the key to check
        :return: bool, True if the key is correct, False otherwise
        """
        kdf = Scrypt(
            salt=salt,
            length=32,
            n=2**14,
            r=256,
            p=1,
        )
        """
        Verify checks whether deriving a new key from the supplied 
        key_material generates the same key as the expected_key, and 
        raises an exception if they do not match. This can be used 
        for checking whether the password a user provides matches 
        the stored derived key.
        """

        try:
            kdf.verify(key_material, expected_key)
        except:
            print("Error: The key is incorrect")
            return False
        else:
            return True

    def verify_text_key(self, salt, key_material, expected_key):
        """
        Verify that a plain text key is correct
        """
        self.verify_key(salt, bytes(key_material, 'utf-8'), expected_key)