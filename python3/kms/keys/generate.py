import os
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.kdf.scrypt import Scrypt

class KeyGeneration:
    def __init__(self):
        self.private_key = None
        self.public_key = None
    
    def generate_key_pair(self, key_size=2048):
        private_key = rsa.generate_private_key(
            public_exponent=65537,
            key_size=key_size
        )
        self.private_key = private_key
        self.public_key = private_key.public_key()

    def export_private_key(self, passphrase:str, pem_file):
        if self.private_key:
            pem = self.private_key.private_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PrivateFormat.PKCS8,
                encryption_algorithm=serialization.BestAvailableEncryption(passphrase.encode())
            )
            with open(pem_file, 'wb') as f:
                f.write(pem)
        else:
            raise ValueError("No private key found!")
            
    def export_public_key(self, pem_file):
        if self.public_key:
            pem = self.public_key.public_bytes(
                encoding=serialization.Encoding.PEM,
                format=serialization.PublicFormat.SubjectPublicKeyInfo
            )
            with open(pem_file, 'wb') as f:
                f.write(pem)
        else:
            raise ValueError("No public key found!")

##### Tested Below This line

    def derive_key(self, password):
        self.salt = os.urandom(16)

        kdf = Scrypt(
            salt=self.salt,
            length=32,
            n=2**14,
            r=256,
            p=1,
        )
        """
        Derive a key from the password
        :return: The binary derived key
        """
        return kdf.derive(password)

    def derive_text_key(self, password):
        """
        Derives a plain text key
        """
        return self.derive_key( bytes(password, 'utf-8') )

    def derived_salt(self):
        """
        Derived salt from derive_key
        :return: The salt
        """
        return self.salt