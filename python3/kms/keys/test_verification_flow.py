from generate import KeyGeneration 
from verification import KeyVerification

# Usage example 
kdf = KeyGeneration()
kvf = KeyVerification()

password_to_check = "password"

key_to_check = kdf.derive_text_key("password")
#print("key_to_check: " + key_to_check )

salt = kdf.derived_salt()
#print("salt: " + salt )

kvf.verify_text_key(salt, password_to_check, key_to_check)
