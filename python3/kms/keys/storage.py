from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.backends import default_backend

class KeyStorage:
    def __init__(self):
        self.private_key = None
        self.public_key = None

    def import_private_key(self, passphrase:str, pem_file):
        with open(pem_file, "rb") as key_file:
            self.private_key = serialization.load_pem_private_key(
                key_file.read(),
                password=passphrase.encode(),
                backend=default_backend()
            )

    def import_public_key(self, pem_file):
        with open(pem_file, "rb") as key_file:
            self.public_key = serialization.load_pem_public_key(
                key_file.read(),
                backend=default_backend()
            )