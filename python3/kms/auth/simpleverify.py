from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf import KeyDerivationFunction

class SimpleVerify:
    """
    SimpleVerify class provides a simple way to derive and verify a key from a password.

    :param password: A bytes object representing the password.
    :param salt: A bytes object representing a random value, used to make the derived key unique.
    :param iterations: An integer representing the number of iterations to perform.
    :param length: An integer representing the length of the derived key.
    :param algorithm: An algorithm from the cryptography.hazmat.primitives.hashes module.
    """

    def __init__(self, password, salt, iterations, length, algorithm):
        self.supported_algorithms = [hashes.SHA256, hashes.SHA384, hashes.SHA512, hashes.SHA3_256, hashes.SHA3_384, hashes.SHA3_512]
        self._validate_algorithm(algorithm)
        self.password = password
        self.salt = salt
        self.iterations = iterations
        self.length = length
        self.algorithm = algorithm
        self.kdf = KeyDerivationFunction(
            algorithm=self.algorithm,
            length=self.length,
            salt=self.salt,
            iterations=self.iterations,
            backend=default_backend()
        )

    def _validate_algorithm(self, algorithm):
        """
        Validate the algorithm is supported by the class
        :param algorithm: An algorithm from the cryptography.hazmat.primitives.hashes module
        :raises: ValueError if the algorithm is not supported
        """
        if algorithm not in self.supported_algorithms:
            raise ValueError(f"{algorithm} is not a supported algorithm. Supported algorithms are {self.supported_algorithms}")

    def derive_key(self):
        """
        Derive a key from the password
        :return: The derived key
        """
        key = self.kdf.derive(self.password)
        return key

    def verify_key(self, password_to_check, key_to_check):
        """
        Verify that a key is correct
        :param password_to_check: bytes, the password to check
        :param key_to_check: bytes, the key to check
        :return: bool, True if the key is correct, False otherwise
        """
        if self.kdf.verify(password_to_check, key_to_check):
            return True
        else:
            return False