#

class Authentication:
    def __init__(self):
        self.users = {}
    
    def add_user(self, username:str, password:str, roles:list):
        self.users[username] = {
            'password': password,
            'roles': roles
        }
        
    def authenticate(self, username:str, password:str):
        if username in self.users:
            user = self.users[username]
            if user['password'] == password:
                return True
        return False
    
    def authorize(self, username:str, role:str):
        if username in self.users:
            user = self.users[username]
            if role in user['roles']:
                return True
        return False