from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf import KeyDerivationFunction

class SimpleVerify:
    """
    SimpleVerify class provides a simple way to derive and verify a key from a password.

    :param password: A bytes object representing the password.
    :param salt: A bytes object representing a random value, used to make the derived key unique.
    :param iterations: An integer representing the number of iterations to perform.
    :param length: An integer representing the length of the derived key.
    :param algorithm: An algorithm from the cryptography.hazmat.primitives.hashes module.
    """

    def __init__(self, password, salt, iterations, length, algorithm):
        self.password = password
        self.salt = salt
        self.iterations = iterations
        self.length = length
        self.algorithm = algorithm
        self.kdf = KeyDerivationFunction()

    def derive_key(self):
        """
        Derive a key from the password
        :return: The derived key
        """
        key = self.kdf.derive(self.password)
        return key

    def verify_key(self, password_to_check, key_to_check):
        """
        Verify that a key is correct
        :param password_to_check: bytes, the password to check
        :param key_to_check: bytes, the key to check
        :return: bool, True if the key is correct, False otherwise
        """
        if self.kdf.verify(password_to_check, key_to_check):
            return True
        else:
            return False