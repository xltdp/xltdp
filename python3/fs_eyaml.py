import pyeyaml
import yaml

# # Create an instance of the fs_eyaml class
# eyaml = fs_eyaml('public_key.pem', 'private_key.pem')

# # Create some data to be encrypted
# data = {
#     'key1': 'value1',
#     'key2': 'value2',
# }

# # Encrypt and write the data to a YAML file
# eyaml.create(data, 'data.yaml')

# # Read and decrypt the data from the YAML file
# decrypted_data = eyaml.read('data.yaml')
# print(decrypted_data)

# # Update the data and write it back to the YAML file
# eyaml.update({'key1': 'new value'}, 'data.yaml')

# # Read and decrypt the updated data from the YAML file
# decrypted_data = eyaml.read('data.yaml')
# print(decrypted_data)

# # Delete a key from the data and write it back to the YAML

class fs_eyaml:
    def __init__(self, public_key_file, private_key_file):
        self.public_key_file = public_key_file
        self.private_key_file = private_key_file

    def create(self, data, file):
        # Encrypt the data using the public key
        encrypted_data = {k: pyeyaml.encrypt(v, self.public_key_file) for k, v in data.items()}

        # Write the encrypted data to the YAML file
        with open(file, 'w') as f:
            yaml.dump(encrypted_data, f)

    def read(self, file):
        # Read the encrypted data from the YAML file
        with open(file, 'r') as f:
            encrypted_data = yaml.load(f, Loader=yaml.SafeLoader)

        # Decrypt the data using the private key
        decrypted_data = {k: pyeyaml.decrypt(v, self.private_key_file) for k, v in encrypted_data.items()}

        return decrypted_data

    def update(self, data, file):
        # Read the encrypted data from the YAML file
        with open(file, 'r') as f:
            encrypted_data = yaml.load(f, Loader=yaml.SafeLoader)

        # Decrypt the data using the private key
        decrypted_data = {k: pyeyaml.decrypt(v, self.private_key_file) for k, v in encrypted_data.items()}

        # Update the decrypted data with the new data
        decrypted_data.update(data)

        # Encrypt the updated data using the public key
        encrypted_data = {k: pyeyaml.encrypt(v, self.public_key_file) for k, v in decrypted_data.items()}

        # Write the encrypted data back to the YAML file
        with open(file, 'w') as f:
            yaml.dump(encrypted_data, f)

    def delete(self, key, file):
        # Read the encrypted data from the YAML file
        with open(file, 'r') as f:
            encrypted_data = yaml.load(f, Loader=yaml.SafeLoader)

        # Decrypt the data using the private key
        decrypted_data = {k: pyeyaml.decrypt(v, self.private_key_file) for k, v in encrypted_data.items()}

        # Delete the specified key from the decrypted data
        decrypted_data.pop(key, None)

        # Encrypt the updated data using the public key
        encrypted_data = {k: pyeyaml.encrypt(v, self.public_key_file) for k, v in decrypted_data.items()}

        # Write the encrypted data back to the YAML file
        with open(file, 'w') as f:
            yaml.dump(encrypted_data, f)        