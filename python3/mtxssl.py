import base64
import hashlib
import hmac

from saltssl import SaltSSL

# # Create an instance of the MtxSSL class
# mtxssl = MtxSSL('/path/to/certificate.pem', 'password')

# # Create a new password hash using the SHA256 hash function
# hash = mtxssl.create_hash()
# print(f'Password hash: {hash}')

# # Verify that the given password hash matches the original password
# is_match = mtxssl.verify_hash(hash)
# if is_match:
#     print('The password hash matches the original password')
# else:
#     print('The password hash does not match the original password')
#
# Password hash: <hash>
# The password hash matches the original password

class MtxSSL:
    def __init__(self, salt_file, password):
        self.salt_file = salt_file
        self.password = password
        self.saltssl = SaltSSL(self.salt_file)

    def create_hash(self, hash_function='sha1'):
        """Create a password hash using the salt file and the specified hash function."""
        salt = self.saltssl.compute_thumbprint(hash_function)
        hash_obj = hmac.new(salt.encode(), self.password.encode(), hash_function)
        return base64.b64encode(hash_obj.digest()).decode()

    def verify_hash(self, hash):
        """Verify that the given hash matches the password using the salt file."""
        computed_hash = self.create_hash()
        return hmac.compare_digest(hash, computed_hash)