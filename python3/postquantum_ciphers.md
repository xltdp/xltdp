# Usage examples for the postquantum_ciphers.py module
## McEliece
Encrypting and decrypting data using McEliece:
```
# Import the PostQuantumCiphers class
from postquantum_ciphers import PostQuantumCiphers

# Create a PostQuantumCiphers object with the password, salt, and algorithm
cipher = PostQuantumCiphers('password', 'salt', 'mceliece')

# Encrypt the data
ciphertext = cipher.encrypt('This is the data to be encrypted', 'salt')
print(f'Encrypted data: {ciphertext}')

# Decrypt the ciphertext
data = cipher.decrypt(ciphertext, 'salt')
print(f'Decrypted data: {data}')
```
This code encrypts the data `'This is the data to be encrypted'` using McEliece,  
and then decrypts the resulting ciphertext using the original password and salt.  
The output will be `'Decrypted data: This is the data to be encrypted'`.  

Encrypting and decrypting file using McEliece:
```
# Import the PostQuantumCiphers class
from postquantum_ciphers import PostQuantumCiphers

# Create a PostQuantumCiphers object with the password, salt, and algorithm
cipher = PostQuantumCiphers('password', 'salt', 'mceliece')

# Read the contents of the file to be encrypted
with open('data.txt', 'r') as f:
    data = f.read()

# Encrypt the data
ciphertext = cipher.encrypt(data, 'salt')

# Write the ciphertext to a file
with open('ciphertext.bin', 'wb') as f:
    f.write(ciphertext)

# Read the ciphertext from the file
with open('ciphertext.bin', 'rb') as f:
    ciphertext = f.read()

# Decrypt the ciphertext
data = cipher.decrypt(ciphertext, 'salt')

# Write the decrypted data to a file
with open('decrypted.txt', 'w') as f:
    f.write(data)

```
This code reads the contents of the file data.txt, encrypts it using McEliece,  
and writes the resulting ciphertext to the file ciphertext.bin. It then reads the  
ciphertext from ciphertext.bin, decrypts it using McEliece, and writes the  
resulting decrypted data to the file decrypted.txt.  
  
Verifying data using McEliece:
```
# Import the PostQuantumCiphers class
from postquantum_ciphers import PostQuantumCiphers

# Create a PostQuantumCiphers object with the password, salt, and algorithm
cipher = PostQuantumCiphers('password', 'salt', 'mceliece')

# Encrypt the data
ciphertext = cipher.encrypt('This is the data to be encrypted', 'salt')
print(f'Encrypted data: {ciphertext}')

# Decrypt the ciphertext
data = cipher.decrypt(ciphertext, 'salt')
print(f'Decrypted data: {data}')
```
This code encrypts the data `'This is the data to be encrypted'` using McEliece, and then verifies the data using the original data and the resulting ciphertext. The output will be `'Is data valid: True'` if the data is valid, and `'Is data valid: False'` if the data is not valid.
  
## NTRUEncrypt
Encrypting and decrypting data using NTRUEncrypt:
```
# Import the PostQuantumCiphers class
from postquantum_ciphers import PostQuantumCiphers

# Create a PostQuantumCiphers object with the password, salt, and algorithm
cipher = PostQuantumCiphers('password', 'salt', 'ntruencrypt')

# Encrypt the data
ciphertext = cipher.encrypt('This is the data to be encrypted', 'salt')
print(f'Encrypted data: {ciphertext}')

# Decrypt the ciphertext
data = cipher.decrypt(ciphertext, 'salt')
print(f'Decrypted data: {data}')
```
This code encrypts the data `'This is the data to be encrypted'` using NTRUEncrypt,  
and then decrypts the resulting ciphertext using the original password and salt.  
The output will be `'Decrypted data: This is the data to be encrypted'`.  
  
Encrypting and decrypting a file using NTRUEncrypt:
```
# Import the PostQuantumCiphers class
from postquantum_ciphers import PostQuantumCiphers

# Create a PostQuantumCiphers object with the password, salt, and algorithm
cipher = PostQuantumCiphers('password', 'salt', 'ntruencrypt')

# Read the contents of the file to be encrypted
with open('data.txt', 'r') as f:
    data = f.read()

# Encrypt the data
ciphertext = cipher.encrypt(data, 'salt')

# Write the ciphertext to a file
with open('ciphertext.bin', 'wb') as f:
    f.write(ciphertext)

# Read the ciphertext from the file
with open('ciphertext.bin', 'rb') as f:
    ciphertext = f.read()

# Decrypt the ciphertext
data = cipher.decrypt(ciphertext, 'salt')

# Write the decrypted data to a file
with open('decrypted.txt', 'w') as f:
    f.write(data)

```
This code reads the contents of the file data.txt, encrypts it using NTRUEncrypt,  
and writes the resulting ciphertext to the file ciphertext.bin. It then reads the  
ciphertext from ciphertext.bin, decrypts it using NTRUEncrypt, and writes the  
resulting decrypted data to the file decrypted.txt.  
  
Verifying data using NTRUEncrypt
```
# Import the PostQuantumCiphers class
from postquantum_ciphers import PostQuantumCiphers

# Create a PostQuantumCiphers object with the password, salt, and algorithm
cipher = PostQuantumCiphers('password', 'salt', 'ntruencrypt')

# Encrypt the data
ciphertext = cipher.encrypt('This is the data to be encrypted', 'salt')
print(f'Encrypted data: {ciphertext}')

# Verify the data
is_valid = cipher.verify('This is the data to be encrypted', ciphertext, 'salt')
print(f'Is data valid: {is_valid}')
```
This code encrypts the data `'This is the data to be encrypted'` using NTRUEncrypt, and then verifies the data using the original data and the resulting ciphertext. The output will be `'Is data valid: True'` if the data is valid, and `'Is data valid: False'` if the data is not valid.  

## NewHope
Encrypting and decrypting a data using NewHope:
```
# Import the PostQuantumCiphers class
from postquantum_ciphers import PostQuantumCiphers

# Create a PostQuantumCiphers object with the password, salt, and algorithm
cipher = PostQuantumCiphers('password', 'salt', 'newhope')

# Encrypt the data
ciphertext = cipher.encrypt('This is the data to be encrypted', 'salt')
print(f'Encrypted data: {ciphertext}')

# Decrypt the ciphertext
data = cipher.decrypt(ciphertext, 'salt')
print(f'Decrypted data: {data}')
```
This code encrypts the data `'This is the data to be encrypted'` using NewHope,  
and then decrypts the resulting ciphertext using the original password and salt.  
The output will be `'Decrypted data: This is the data to be encrypted'`.  
  
Encrypting and decrypting a file using NewHope:
```
# Import the PostQuantumCiphers class
from postquantum_ciphers import PostQuantumCiphers

# Create a PostQuantumCiphers object with the password, salt, and algorithm
cipher = PostQuantumCiphers('password', 'salt', 'newhope')

# Read the contents of the file to be encrypted
with open('data.txt', 'r') as f:
    data = f.read()

# Encrypt the data
ciphertext = cipher.encrypt(data, 'salt')

# Write the ciphertext to a file
with open('ciphertext.bin', 'wb') as f:
    f.write(ciphertext)

# Read the ciphertext from the file
with open('ciphertext.bin', 'rb') as f:
    ciphertext = f.read()

# Decrypt the ciphertext
data = cipher.decrypt(ciphertext, 'salt')

# Write the decrypted data to a file
with open('decrypted.txt', 'w') as f:
    f.write(data)
```
This code reads the contents of the file data.txt, encrypts it using NewHope,  
and writes the resulting ciphertext to the file ciphertext.bin. It then reads the  
ciphertext from ciphertext.bin, decrypts it using NewHope, and writes the  
resulting decrypted data to the file decrypted.txt.  
  
Verifying data using NewHope:
```
# Import the PostQuantumCiphers class
from postquantum_ciphers import PostQuantumCiphers

# Create a PostQuantumCiphers object with the password, salt, and algorithm
cipher = PostQuantumCiphers('password', 'salt', 'newhope')

# Encrypt the data
ciphertext = cipher.encrypt('This is the data to be encrypted', 'salt')
print(f'Encrypted data: {ciphertext}')

# Verify the data
is_valid = cipher.verify('This is the data to be encrypted', ciphertext, 'salt')
print(f'Is data valid: {is_valid}')
```
This code encrypts the data `'This is the data to be encrypted'` using NewHope,  
and then verifies the data using the original data and the resulting ciphertext.  
The output will be `'Is data valid: True'` if the data is valid, and  
`'Is data valid: False'` if the data is not valid.  
